#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>



int main(int argc , char ** argv){
  int maximo;
  int posmax;
  int tamano;
  int numeros[100]={0};
  char buff[100]={0};
  int contador=0;
  int stop=0;
  int flag=1;
  int iterador;
  int numerotomado;
  int suma=0;
  int opt;
  int noArgumentos;

  while ((opt = getopt (argc, argv, "n:")) != -1){
    switch(opt){
      case 'n': 
        stop=atoi(optarg);
        flag=0;
        break;
      case '?':
        default:
          printf("Uso correcto:\n");
          printf("%s -n <numero de valores a usar>\n",argv[0]);
          return -1;
    }

  }

  if(argc>optind){
    printf("Uso correcto:\n");
    printf("%s -n <numero de valores a usar>\n",argv[0]);
    return -1;

  }

  while(contador < stop || flag) {
		fgets(buff,100,stdin);
		if(strcmp(buff, "x\n") == 0){
			break;
		}
		else {
			numerotomado = atoi(buff);
			numeros[contador] = numerotomado;
      suma=suma+numeros[contador];
			contador ++;
			}
	}
  maximo=numeros[0];
  for(iterador = 0; iterador < contador; iterador ++) {
		if(numeros[iterador] > maximo) {
			maximo = numeros[iterador];
      posmax=iterador;
		}
	}

printf("max\tpos\tsum\n%d\t%d\t%d\n",maximo,posmax,suma);
return 0;

}
